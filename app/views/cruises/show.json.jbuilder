json.extract! @event, :id, :title, :slug, :venue_id, :start_at, :end_at, :type, :content, :created_at, :updated_at
